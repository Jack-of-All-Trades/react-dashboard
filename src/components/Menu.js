import React from 'react'
import { Link, IndexLink } from 'react-website'
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';

import './Menu.scss';
const styles = theme => ({
	button: {
	  margin: theme.spacing.unit,
	  width: 150,	  
	},
	input: {
	  display: 'none',
	},
	menu: {
		textAlign: 'center'
	},
  });

function Menu(props)
{
	const {classes} = props;	
	return (
		<div className={classes.menu}>
		</div>
	)
}

Menu.propTypes = {
	classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Menu);